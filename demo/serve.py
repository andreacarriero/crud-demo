from demo.wsgi import application
from django.conf import settings

import cherrypy
import os

server_config = {
    'global': {
        'server.socket_host': '0.0.0.0',
        'server.socket_port': 8000,
        'server.thread_pool': 10,
        'log.screen': True,
        'log.error_file': '/tmp/demo.error.log',
        'log.access_file': '/tmp/demo.access.log',
        'environment': 'production',
    }
}

application_config = {
    '/': {
        'tools.staticdir.root': os.path.dirname(__file__)
    },
}

# STATIC FILES
static_config = {
    '/': {
        'tools.staticdir.on': True,
        'tools.staticdir.dir': settings.STATIC_ROOT,
        'tools.expires.on': True,
        'tools.expires.secs': 86400
    }
}


if __name__ == '__main__':
    cherrypy.server.unsubscribe()
    cherrypy.config.update(server_config)

    cherrypy.tree.mount(None, '', application_config)
    cherrypy.tree.mount(None, '/static', static_config)

    cherrypy.tree.graft(application, '/')

    cherrypy.server.subscribe()
    cherrypy.engine.start()
    cherrypy.engine.block()