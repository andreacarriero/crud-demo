from django.http import JsonResponse
from django.views.decorators.http import require_http_methods, require_GET, require_POST
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string

from .models import Car
from .forms import CarForm

@require_GET
def get_lookup_items(request):
    # Associate brand and fuel int ref to str name
    return JsonResponse({
        'brands': {brand[0]: brand[1] for brand in Car.BRANDS},
        'fuelTypes': {fuelType[0]: fuelType[1] for fuelType in Car.FUELS}
    })

@require_GET
def get_cars(request):
    # Return cars list
    cars = Car.objects.all(); cars = [car.serialize() for car in cars]
    return JsonResponse({'cars': cars})

@require_POST
def add_car(request):
    # Validate and add new car
    data = dict()
    form = CarForm(request.POST)
    if form.is_valid():
        form.save()
        data['valid'] = True
    else:
        data['valid'] = False
        data['errors'] = form.errors

    return JsonResponse(data)


def update_car(request, id):
    # Validate and update car by id
    data = dict()
    car = get_object_or_404(Car, id=id)
    if request.method == 'POST':
        form = CarForm(request.POST, instance=car)
        if form.is_valid():
            form.save()
            data['valid'] = True
        else:
            data['valid'] = False
            data['errors'] = form.errors
    else:
        form = CarForm(instance=car)
        form_html = render_to_string('cars/update_dialog.html', {'form': form}, request=request)
        data['form_html'] = form_html

    return JsonResponse(data)

@require_GET
def delete_car(request, id):
    # Delete car by id
    car = get_object_or_404(Car, id=id)
    car.delete()
    return JsonResponse({'valid': True})