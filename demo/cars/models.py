from django.db import models

# Create your models here.
class Car(models.Model):
    BRAND_fiat = 0
    BRAND_alfaRomeo = 1
    BRAND_BMW = 2
    BRAND_toyota = 3

    BRANDS = (
        (BRAND_fiat, 'Fiat'),
        (BRAND_alfaRomeo, 'Alfa Romeo'),
        (BRAND_BMW, 'BMW'),
        (BRAND_toyota, 'Toyota')
    )

    FUEL_gasoline = 0
    FUEL_diesel = 1
    FUEL_gpl = 2

    FUELS = (
        (FUEL_gasoline, 'Gasoline'),
        (FUEL_diesel, 'Diesel'),
        (FUEL_gpl, 'GPL')
    )

    brand = models.PositiveSmallIntegerField(choices=BRANDS, verbose_name='Brand')
    model = models.CharField(max_length=30, verbose_name='Model')
    fuelType = models.PositiveSmallIntegerField(choices=FUELS, verbose_name='Fuel Type')
    licensePlate = models.CharField(max_length=8, unique=True, verbose_name='License Plate')

    def clean(self):
        self.licensePlate = self.licensePlate.strip().replace(' ', '').upper()

    def serialize(self):
        return {
            'id': self.id,
            'brand': self.brand,
            'model': self.model,
            'fuelType': self.fuelType,
            'licensePlate': self.licensePlate
        }