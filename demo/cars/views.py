from django.shortcuts import render
from .forms import CarForm

# Create your views here.
def home(request):
    return render(request, 'cars/list.html', {'form': CarForm()})