"""demo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from cars import views as cars_views
from cars import ajax as cars_ajax

urlpatterns = [
    url(r'^$', cars_views.home, name='home'),


    url(r'^ajax/lookupitems$', cars_ajax.get_lookup_items, name='get_lookup_items'),
    url(r'^ajax/cars$', cars_ajax.get_cars, name='get_cars'),
    url(r'^ajax/cars/add$', cars_ajax.add_car, name='add_car'),
    url(r'^ajax/car/(?P<id>\d+)/update$', cars_ajax.update_car, name='update_car'),
    url(r'^ajax/car/(?P<id>\d+)/delete$', cars_ajax.delete_car, name='delete_car'),

    url(r'^admin/', admin.site.urls),
]
