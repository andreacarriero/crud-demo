// Associate brand and fuel type int ref to name
$.ajax({
    url: 'ajax/lookupitems',
    dataType: 'json',
    success: function(data) {
        window.lookupItems = data;
    }
});

// Capture addCarForm submission
$(function() {
    $('#addCarForm').on('submit', function(e) {
        e.preventDefault();
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.valid) {
                    $('#addCarModal').modal('toggle');
                    form.trigger('reset');
                    getCars();
                } else {
                    $.each(data.errors, function(k, v) {
                        $('#addCarForm input[name=' + k + ']').after('<span style="color: red">' + v + '</span>');
                    });
                }
            }
        });
        return false;
    });
});

// Capture updateCarForm submission
function catchUpdate() {
    $('#updateCarForm').on('submit', function(e) {
        e.preventDefault();
        var form = $(this);
        $.ajax({
            url: form.attr("action"),
            data: form.serialize(),
            type: form.attr("method"),
            dataType: 'json',
            success: function (data) {
                if (data.valid) {
                    $('#updateCarModal').modal('toggle');
                    form.trigger('reset');
                    getCars();
                } else {
                    $.each(data.errors, function(k, v) {
                        $('#updateCarForm input[name=' + k + ']').after('<span style="color: red">' + v + '</span>');
                    });
                }
            }
        });
        return false;
    });
}

// Get car list and insert records in table
function getCars() {
    $.ajax({
        url: '/ajax/cars',
        dataType: 'json',
        success: function(data) {
            $('#carsTable > tbody tr').remove();
            if (data.cars.length > 0) {
                $.each(data.cars, function(i, item) {
                    var $tr = $('<tr>').append(
                        $('<td>').text(item.id),
                        $('<td>').text(window.lookupItems.brands[item.brand]),
                        $('<td>').text(item.model),
                        $('<td>').text(window.lookupItems.fuelTypes[item.fuelType]),
                        $('<td>').text(item.licensePlate),
                        $('<td>').html(
                            '<div class="text-center">'
                            + '<button type="button" class="btn btn-warning btn-sm btn-fixedsize" onclick="loadForm(' + item.id + ')">Edit</button>'
                            + ' '
                            + '<button type="button" class="btn btn-danger btn-sm btn-fixedsize" onclick="deleteCar(' + item.id + ')">Delete</button>'
                            + '</div>'
                        )
                    )
                    .appendTo('#carsTable');
                });
            } else {
                var $tr = $('<tr>').append(
                    $('<td colspan="6" class="text-center">').text("No cars")
                )
                .appendTo('#carsTable')
            }
        },

        error: function(data) {
            alert("Error during cars loading")
        }
    });
}

// Populate form with car-to-update details
function loadForm(id) {
    $.ajax({
        url: '/ajax/car/' + id + '/update',
        dataType: 'json',
        success: function(data) {
            $('#updateCarModal').modal('toggle');
            $('#updateCarModalContent').html(data['form_html']);
            catchUpdate();
        },

        error: function() {
            alert("Error on car retriving");
        }
    })
}

// Request car deletion confirmation and then delete
function deleteCar(id) {
    $('#deleteCarModal').modal('toggle');
    $('#deleteCarModalContent').html('Do you really want to delete car #' + id + '?')
    $('#carDeletionConfirmButton').one('click', function() {
        $.ajax({
            url: '/ajax/car/' + id + '/delete',
            dataType: 'json',
            success: function() {
                $('#deleteCarModal').modal('toggle');
                getCars();
            },

            error: function() {
                alert("Error on car deletion");
            }
        })
    });
}