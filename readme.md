#Cars CRUD demo app

##How to set dev environment
1. Install `python`, `python-pip` and `virtualenv`
2. Clone git repository
3. `cd` in project directory
4. Build virtual environment with `virtualenv venv -p python3`
5. Activate virtual env with `python venv/bin/activate`
6. Install requirements with `pip install -r requirements.txt`
7. `cd` in `demo` project directory
8. Setup database with `python manage.py makemigrations && python manage.py migrate`
7. Run dev server `python manage.py runserver`

##How to run production server
1. `cd` in `demo` project directory
2. Set env variable for production with `export DEMO_ENV=PRODUCTION`
3. Run `../venv/bin/python3 server.py`

##Implemented features:

1. Car model with brand, model, fuel type and license plate
2. CRUD
3. Ajax requests
4. Error handlers